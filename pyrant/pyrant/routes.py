# from pyramid.httpexceptions import (
#     HTTPNotFound,
#     HTTPFound,
# )
from pyramid.security import (
    Allow,
    Everyone,
)


# from .models import Page


def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=0)
    config.add_route('home', '/')
    config.add_route('about', '/about')
    config.add_route('admin', '/admin')
    config.add_route('blog', '/blog/{year:\d+}/{month:\d+}/{day:\d+}/{slug}')
    config.add_route('blog_action', '/blog/{action}',
                      factory=BlogPostFactory)
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')


class BlogPostFactory(object):
    __acl__ = [(Allow, Everyone, 'view'),
               (Allow, 'role:admin', 'create'),
               (Allow, 'role:admin', 'edit'), ]

    def __init__(self, request):
        pass
