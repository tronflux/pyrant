from wtforms import (Form, 
                     StringField, 
                     TextAreaField, 
                     BooleanField, 
                     DateTimeField, 
                     validators,
                     )
from wtforms import IntegerField
from wtforms.widgets import HiddenInput
import datetime


DATE_FORMATTER = '%d %b %Y'


strip_filter = lambda x: x.strip() if x else None


class BlogCreateForm(Form):
    title = StringField('Title', [validators.Length(min=1, max=255)],
                        filters=[strip_filter])

    attitude = BooleanField('Attitude', [validators.Optional()], default=True)

    published = BooleanField('Published', [validators.Optional()], default=False)

    link_post = BooleanField('Link Post', [validators.Optional()], default=False)

    link = StringField('Link', [validators.Optional()],
                        filters=[strip_filter])

    created = DateTimeField('Created', [validators.Optional()])

    body = TextAreaField('Contents', [validators.Length(min=1)],
                         filters=[strip_filter])

    name = TextAreaField('Name', [validators.Optional()])


class BlogUpdateForm(BlogCreateForm):
    id = IntegerField(widget=HiddenInput())
