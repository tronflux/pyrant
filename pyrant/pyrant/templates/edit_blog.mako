<%inherit file="layout.mako"/>

<%block name="content">

<!-- TniyMCE Editor code -->
<script src="${ request.static_url('pyrant:static/tinymce/tinymce.min.js') }"></script>
<script>
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>



<article id="latest-article" class="container">

        % if action =='edit':
            <h2>edit post</h2>
        % else:
            <h2>new post</h2>
        % endif

 ## <p class="post-info">by <span>admin</a></span> &nbsp;
 ## filed in <span><a href="#">test</a></span>
 </p>

 <div class="dcontent cf">

<p>

    <form action="${request.route_url('blog_action',action=action)}" method="post" class="form">
        % if action =='edit':
            ${ form.id() }
        % endif

        % for error in form.title.errors:
            <div class="error">${ error }</div>
        % endfor

        <div class="form-group">
            <label for="title">${ form.title.label }</label>
            ${ form.title(class_='form-control') }
        </div>

        <div class="form-group">
            <label for="attitude">${ form.attitude.label }</label>
            ${ form.attitude(class_='form-control') }
        </div>


        <div class="form-group">
            <label for="published">${ form.published.label }</label>
            ${ form.published(class_='form-control') }
        </div>

        <div class="form-group">
            <label for="link_post">${ form.link_post.label }</label>
            ${ form.link_post(class_='form-control') }
        </div>

        <div class="form-group">
            <label for="link">${ form.link.label }</label>
            ${ form.link(class_='form-control') }
        </div>

        <div class="form-group">
            <label for="created">${ form.created.label }</label>
            ${ form.created(class_='form-control') }
        </div>

        % for error in form.body.errors:
            <div class="error">${ error }</div>
        % endfor

        <div class="form-group">
            <label for="body">${ form.body.label }</label>
            ${ form.body(class_='form-control') }
        </div>

        <div class="form-group">
            <label></label>
            <button type="submit" class="btn btn-default">Submit</button>
        </div>


    </form>
    <p><a href="${ request.route_url('admin') }">Go Back</a></p>

</p>

 </div>
</article>


</%block>