<%inherit file="layout.mako"/>

<%block name="content">

    <link href="${ request.static_url('pyrant:static/css/tables.css') }" rel="stylesheet">

<article id="latest-article" class="container">
 <h2>admin</h2>


<p><a href="${ request.route_url('blog_action',action='create') }">New Post</a></p>

<p>
<table class="bordered">
<thead>
  <tr>
    <th>id</th>
    <th>title</th>
    <th>author</th>
    <th>published</th>
    <th>action</th>
  </tr>
</thead>
<tbody>
% for item in posts:
    <tr> 
    <td>${ item.id }</td>
    <td>${ item.title }</td>
    <td>${ item.creator.name }</td>
    <td>${ item.created }</td>
    <td>
    <a href="${ request.route_url('blog_action', action='edit',
        _query={'id':item.id}) }">edit post</a>
    </td>
</tr>
% endfor
</tbody>
</table>
</p>

</article>
</%block>