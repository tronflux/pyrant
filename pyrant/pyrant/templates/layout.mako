<!DOCTYPE html>
<html lang="en">
    <head>
      ## <title>${ page_title }</title>
        <title><%block name="subtitle"/>Actually Sprained</title>

       <!--- Basic Page Needs
       ================================================== -->
        <meta charset="utf-8">
        <title>Actually Sprained</title>
        <meta name="description" content="a blog about things and stuff">
        <meta name="author" content="andrew martin">
    <!-- Latest compiled and minified CSS -->
    ## <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    ## <link rel="stylesheet" type="text/css" href="/static/css/center_columns.css">
    ## <link rel="stylesheet" type="text/css" href="/static/css/sads.css">
    ## <link rel="stylesheet" type="text/css" href="/static/css/magnus.css">
    ## <link rel="stylesheet" type="text/css" href="/static/css/search.css">

    <!-- Custom styles for this scaffold -->
    ## <link href="${ request.static_url('pyrant:static/theme.css') }" rel="stylesheet">

    <link href="${ request.static_url('pyrant:static/css/base.css') }" rel="stylesheet">
    <link href="${ request.static_url('pyrant:static/css/layout.css') }" rel="stylesheet">
    <link href="${ request.static_url('pyrant:static/css/pygments.css') }" rel="stylesheet">
    ## <link href="${ request.static_url('pyrant:static/fonts/fontawesome/on-server/css/fontawesome.min.css') }" rel="stylesheet">
    ## fontawesome version is 5.0.6

    <!-- TniyMCE Editor code -->
    ## <script src="/static/tinymce/tinymce.min.js"></script>

    <!--Floating Headers-->
    ## <script src="/static/jquery.floatThead.min.js"></script>

    </head>
    <body>
        <div id="content-wrap">

            <%include file="header.mako"/>

            <div class="content">
                ## <div class="row">
                ##     <div class="col-md-12">
                ##         % if request.user is None:
                ##             <p class="pull-right">
                ##             <a href="${ request.route_url('login') }">Login</a>
                ##             </p>
                ##         % else:
                ##             <p class="pull-right">
                ##             ${request.user.name} <a href="${request.route_url('logout')}">Logout</a>
                ##             </p>
                ##         % endif
                ##     </div>
                ## </div>
                <div class="row">
                    <div class="col-md-12">
                        ## <div class="text-center">

                          <%block name="content"/>
                          
                        </div>
                    </div>
                </div>
            </div>

            ## <div>
            ## <%include file="search.mako"/>
            ## </div>

            <div>
            <%include file="footer.mako"/>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

       ## <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
       ## <script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>


       ## <script src="${ request.static_url('pyrant:static/FreshPick20/js/custom.js') }"></script>


    ## <!-- jQuery library -->
    ## <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    ## <!-- Latest compiled JavaScript -->
    ## <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    ## <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    </body>
</html>