<%inherit file="layout.mako"/>

<%block name="content">

      <!-- latest article
      ================================================== -->
    % if paginator.items:
        <% 
            latest_entry = paginator.items.pop(0)
            DATE_FORMATTER = '%d %b %Y'
            date = latest_entry.created.strftime(DATE_FORMATTER)
            date_list = date.split(' ')
            URL_FORMATTER = '%Y-%m-%d'
            url_year, url_month, url_day = latest_entry.created.strftime(URL_FORMATTER).split('-')

        %>
            <article id="latest-article" class="container">
             <h2>
             % if latest_entry.link_post is True:
             <a href="${ latest_entry.link }">${ latest_entry.title }
             % else:
             <a href="${ request.route_url('blog', year=url_year, month=url_month, day=url_day, slug=latest_entry.slug) }">${ latest_entry.title }
             % endif

             % if latest_entry.attitude is True:
             <img src="${ request.static_url('pyrant:static/images/rebels.png') }" height="40" width="40"></a>
             % else:
             <img src="${ request.static_url('pyrant:static/images/imperials.png') }" height="40" width="40"></a>
             % endif
             </h2>
             <p class="post-info">by <span>${ latest_entry.creator.name }</a></span> &nbsp;
             </p>

             <div class="dcontent cf">
                <p>${ latest_entry.body | n }</p>

                ## <p class="continue cf"><a class="button" href="${ request.route_url('blog', id=latest_entry.id, slug=latest_entry.slug) }">Continue Reading</a></p>

                <div class="post-meta">

                   <p class="dateinfo">
                      ${ date_list[0] }
                      <span class="dmonth">${ date_list[1] }</span>
                      <span class="dyear">${ date_list[2] }</span>
                   </p>
                </div>
             </div>
            </article>
    % else:
        <article id="latest-article" class="container">
        <p>No blog entries found.</p>
        </article>
    % endif

      <!-- more articles
      ================================================== -->
    <div id="more-articles" class="container">
    % for entry in paginator.items:

        <% 
            DATE_FORMATTER = '%d %b %Y'
            date = entry.created.strftime(DATE_FORMATTER)
            URL_FORMATTER = '%Y-%m-%d'
            url_year, url_month, url_day = latest_entry.created.strftime(URL_FORMATTER).split('-')
        %>


        <article class="cf">
            <div class="grid4 a-left first">
               <h3>
             % if entry.link_post is True:
                <a href="${ entry.link }">${ entry.title }
                </a>
             % else:
                <a href="${ request.route_url('blog', year=url_year, month=url_month, day=url_day, slug=entry.slug) }">
                ${ entry.title }
                </a>
             % endif
              </h3>
               <p>${ date }
                <br />
                <br />
             % if entry.attitude:
                <a href="${ request.route_url('blog', year=url_year, month=url_month, day=url_day, slug=entry.slug) }">
                <img src="${ request.static_url('pyrant:static/images/rebels.png') }"
                height="18" width="18" alt="rebels" title="permalink">
                </a>
             % else:
                <a href="${ request.route_url('blog', year=url_year, month=url_month, day=url_day, slug=entry.slug) }">
                <img src="${ request.static_url('pyrant:static/images/imperials.png') }"
                height="18" width="18" alt="rebels" title="permalink">
                </a>
             % endif
                </p>


            </div>
            <div class="grid8 a-right">
            <p>
            ${ entry.body | n }
            </p>
         </article>

    % endfor
    </div>

    ## ${ paginator.pager() | n }

    ## <p><a href="${ request.route_url('blog_action',action='create') }">
    ##     Create a new blog entry</a></p>

</%block>
