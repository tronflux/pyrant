      <!-- search section
      ================================================== -->
      <div id="search-section" class="container">
         <form class="cf">

            <div class="grid4 a-left first">
               <label for="regularInput">Search the site</label>
            </div>

            <div class="grid8 a-right">
               <input type="text" id="regularInput">
               <button type="submit">GO</button>
            </div>

         </form>
      </div>

   </div> <!-- end content-wrap -->