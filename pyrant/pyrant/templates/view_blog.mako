<%inherit file="layout.mako"/>

<%block name="content">

<article id="latest-article" class="container">
    % if entry:
        <% 
            DATE_FORMATTER = '%d %b %Y'
            date = entry.created.strftime(DATE_FORMATTER)
            date_list = date.split(' ')
            URL_FORMATTER = '%Y-%m-%d'
            url_year, url_month, url_day = entry.created.strftime(URL_FORMATTER).split('-')
        %>
    % endif

    <h2>
 % if entry.link_post is True:
    <a href="${ entry.link }">${ entry.title }
    % if entry.attitude:
        <img src="${ request.static_url('pyrant:static/images/rebels.png') }" height="38" width="38">
        </a>
    % else:
        <img src="${ request.static_url('pyrant:static/images/imperials.png') }" height="38" width="38">
        </a>
    % endif
    </h2>

% else:
    <a href="${ request.route_url('blog', year=url_year, month=url_month, day=url_day, slug=entry.slug) }">${ entry.title }
    % if entry.attitude:
        <img src="${ request.static_url('pyrant:static/images/rebels.png') }" height="38" width="38">
        </a>
    % else:
        <img src="${ request.static_url('pyrant:static/images/imperials.png') }" height="38" width="38">
        </a>
    % endif
    </h2>
% endif

 ## filed in <span><a href="#">test</a></span>
<p class="post-info">by <span>${ entry.creator.name }</span> &nbsp;

<div class="dcontent cf">
<p>${ entry.body | n }</p>

<div class="post-meta">

   <p class="dateinfo">
      ${ date_list[0] }
      <span class="dmonth">${ date_list[1] }</span>
      <span class="dyear">${ date_list[2] }</span>
   </p>
</div>
</div>
</article>




</%block>