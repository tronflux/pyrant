<nav id="nav-wrap" class="cf">

    <ul id="menu">
        ## <li class="current"><a href="${ request.route_url('home') }">home</a></li>
        <li ${ "class=current" if 'blog' in request.path else "" }><a href="${ request.route_url('home') }">blog</a></li>
        ## <li><a ${ "class=current" if 'about' in request.path else "" } href="/about">dashboards</a></li>
        ## <li><a href="style-demo.htm">Style Demo</a></li>
        ## <li><a href="single-post.htm">Blog</a></li>
        ## <li><a href="archives.htm">Archives</a></li>
        ## <li><a href="#">Dropdown</a>
        ##   <ul>
        ##      <li><a href="#"><span>- </span>Submenu 01</a></li>
        ##      <li><a href="#"><span>- </span>Submenu 02</a></li>
        ##      <li><a href="#"><span>- </span>Submenu 03</a></li>
        ##   </ul>
        ## <li><a href="#">about</a></li>
        <li ${ "class=current" if '/about' == request.path else "" }><a href="${ request.route_url('about') }">about</a></li>
        ## <li ${ "class=current" if '/admin' == request.path else "" }><a href="${ request.route_url('admin') }">admin</a></li>
    </ul> <!-- end #menu -->

</nav>