<%inherit file="layout.mako"/>

<%block name="content">


<article id="latest-article" class="container">

 <h2>about me</h2>
 ## <p class="post-info">by <span>admin</a></span> &nbsp;
 ## filed in <span><a href="#">test</a></span>
 <p></p>

 <div class="dcontent cf">
    <p>Hi, I'm andrew martin. I'm a data engineer and violinist and music theory nerd.
       This is my blog, and you are reading it for some reason. If you have comments
       about anything I'm writing here, feel free to email me at agmartin@gmail.com.
       <br />
       <br />
       This blog is running an engine I made that I call pyrant. Because it was written
       in python and gives me a platform for ranting at the internet.

       I used the pyramid web framework, and the source code is available on my
       <a href="https://bitbucket.org/tronflux/pyrant">bitbucket account.</a>
       I'll get the documentation together eventually.
       <br />
       <br />
       I really like purple. And minimalism. Hope you don't mind.
    </p>

    <p>&nbsp;</p>

 </div>
</article>


</%block>
