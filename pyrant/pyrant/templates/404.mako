<%inherit file="layout.mako"/>

<%block name="content">


<article id="latest-article" class="container">

 <h2>404 error. page not found.</h2>
 <p></p>

 <div class="dcontent cf">
    <p>Oops. That didn't work. Click above to go home or try throwing yourself at the ground and seeing if you can miss.
    It's the only way to fly.
    </p>

    <p>&nbsp;</p>

 </div>
</article>

</%block>
