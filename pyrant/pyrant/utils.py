from pygments import highlight
from pygments import lexers
from pygments import formatters
from bs4 import BeautifulSoup


def highlight_code(html):
    soup = BeautifulSoup(html, "lxml")
    codeblocks = soup.findAll('code')
    for block in codeblocks:
        dialect = block.get('class')[0]
        code = ''.join([item for item in block.contents])
        lexer = lexers.get_lexer_by_name(dialect)
        formatter = formatters.HtmlFormatter()
        code_hl = highlight(code, lexer, formatter)
        block.replaceWith(BeautifulSoup(code_hl, "lxml"))
    return str(soup)
