import re
import sqlalchemy as sa
from docutils.core import publish_parts

from pyramid.compat import escape
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from pyramid.response import Response

from sqlalchemy.exc import DBAPIError

from ..models import User, BlogPost
from ..services.blog_post import BlogPostService

# DATE_FORMATTER = '%d %B %Y'
DATE_FORMATTER = '%d %b %Y'

wikiwords = re.compile(r"\b([A-Z]\w+[A-Z]+\w+)")


@view_config(route_name='home',
             renderer='pyrant:templates/home.mako')
def index_page(request):
    page = int(request.params.get('page', 1))
    paginator = BlogPostService.get_paginator(request, page)

    return {'paginator': paginator}


@view_config(route_name='about',
             renderer='pyrant:templates/about.mako')
def about_page(request):
    return {'one': 'one'}


@view_config(route_name='admin',
             renderer='pyrant:templates/admin.mako', permission='create')
def admin_page(request):
    query = request.dbsession.query(BlogPost).order_by(sa.desc(BlogPost.created)).all()

    return {'posts': query}


# @view_config(route_name='view_wiki')
# def view_wiki(request):
#     next_url = request.route_url('view_page', pagename='FrontPage')hiy
#     return HTTPFound(location=next_url)


# @view_config(route_name='view_page', renderer='../templates/view.mako',
#              permission='view')
# def view_page(request):
#     page = request.context.page

#     def add_link(match):
#         word = match.group(1)
#         exists = request.dbsession.query(Page).filter_by(name=word).all()
#         if exists:
#             view_url = request.route_url('view_page', pagename=word)
#             return '<a href="%s">%s</a>' % (view_url, escape(word))
#         else:
#             add_url = request.route_url('add_page', pagename=word)
#             return '<a href="%s">%s</a>' % (add_url, escape(word))

#     content = publish_parts(page.data, writer_name='html')['html_body']
#     content = wikiwords.sub(add_link, content)
#     edit_url = request.route_url('edit_page', pagename=page.name)
#     return dict(page=page, contents=content, edit_url=edit_url)


# @view_config(route_name='edit_page', renderer='../templates/edit.mako',
#              permission='edit')
# def edit_page(request):
#     page = request.context.page

#     if 'form.submitted' in request.params:
#         page.data = request.params['body']
#         next_url = request.route_url('view_page', pagename=page.name)
#         return HTTPFound(location=next_url)
#     return dict(
#         pagename=page.name,
#         pagedata=page.data,
#         save_url=request.route_url('edit_page', pagename=page.name),
#         )


# @view_config(route_name='add_page', renderer='../templates/edit.mako',
#              permission='create')
# def add_page(request):
#     pagename = request.context.pagename

#     if 'form.submitted' in request.params:
#         body = request.params['body']
#         page = Page(name=pagename, data=body)
#         page.creator = request.user
#         request.dbsession.add(page)
#         next_url = request.route_url('view_page', pagename=pagename)
#         return HTTPFound(location=next_url)
#     save_url = request.route_url('add_page', pagename=pagename)
#     return dict(pagename=pagename, pagedata='', save_url=save_url)


db_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to run the "initialize_pyramid_blogr_db" script
    to initialize your database tables.  Check your virtual
    environment's "bin" directory for this script and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""
