from pyramid.view import view_config
from pyramid.httpexceptions import HTTPNotFound, HTTPFound
from ..models.blog_post import BlogPost
from ..services.blog_post import BlogPostService
from ..forms import BlogCreateForm, BlogUpdateForm
from ..utils import highlight_code

DATE_FORMATTER = '%d %b %Y'
URL_FORMATTER = URL_FORMATTER = '%Y-%m-%d'



@view_config(route_name='blog',
             renderer='pyrant:templates/view_blog.mako')
def blog_view(request):
    # blog_id = int(request.matchdict.get('id', -1))
    blog_name = (request.matchdict.get('slug', -1))
    entry = BlogPostService.by_name(blog_name, request)
    if not entry:
        return HTTPNotFound()
    return {'entry': entry}


@view_config(route_name='blog_action', match_param='action=create',
             renderer='pyrant:templates/edit_blog.mako',
             permission='create',
             )
def blog_create(request):
    entry = BlogPost()
    form = BlogCreateForm(request.POST)
    if request.method == 'POST' and form.validate():
        entry.set_creator_id(request.user.id)
        form.body.data = highlight_code(form.body.data)
        form.populate_obj(entry)
        entry.name = entry.slug
        request.dbsession.add(entry)
        return HTTPFound(location=request.route_url('home'))
    return {'form': form, 'action': request.matchdict.get('action')}


@view_config(route_name='blog_action', match_param='action=edit',
             renderer='pyrant:templates/edit_blog.mako',
             permission='edit',
             )
def blog_update(request):
    blog_id = int(request.params.get('id', -1))
    # blog_name = (request.matchdict.get('slug', -1))
    entry = BlogPostService.by_id(blog_id, request)
    url_year, url_month, url_day = entry.created.strftime(URL_FORMATTER).split('-')
    if not entry:
        return HTTPNotFound()
    form = BlogUpdateForm(request.POST, entry)
    if request.method == 'POST' and form.validate():
        del form.id  # SECURITY: prevent overwriting of primary key
        del form.name # SECURITY: prevent overwriting of name
        form.body.data = highlight_code(form.body.data)
        form.populate_obj(entry)
        return HTTPFound(
            location=request.route_url('blog', year=url_year, month=url_month, day=url_day, slug=entry.slug))
    return {'form': form, 'action': request.matchdict.get('action')}

