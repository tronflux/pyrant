import datetime
from sqlalchemy import (
    Column,
    Integer,
    Unicode,
    UnicodeText,
    DateTime,
    ForeignKey,
    Boolean,
)

from sqlalchemy.orm import relationship
from .meta import Base

from webhelpers2.text import urlify
from webhelpers2.date import distance_of_time_in_words


class BlogPost(Base):
    """docstring for BlogPost"""
    __tablename__ = 'posts'

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(255), unique=True, nullable=False)
    body = Column(UnicodeText, default=u'', unique=False)
    created = Column(DateTime, unique=False, default=datetime.datetime.utcnow, nullable=False)
    published = Column(Boolean, unique=False, default=False, nullable=False)
    link_post = Column(Boolean, unique=False, default=False, nullable=False)
    link = Column(Unicode(640), unique=False, nullable=True, default=u'')
    name = Column(Unicode(255), unique=True, nullable=False)

    # this determines whether I'm griping about something
    # happy about it and the icon that gets displayed
    # True means I like it and displays a Rebels icon with
    # the post. False means I'm critical and displays an
    # empire symbol
    attitude = Column(Boolean, unique=False, default=True)

    creator_id = Column(ForeignKey('users.id'), nullable=False)
    creator = relationship('User', backref='created_posts')

    @property
    def slug(self):
        return urlify(self.title)

    @property
    def created_in_words(self):
        return distance_of_time_in_words(self.created,
                                         datetime.datetime.utcnow())

    def set_creator_id(self, creator_id):
        self.creator_id = creator_id
